<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'fac1c4c77bf1c2c7b9849e19246b4976b9a44b8d',
        'name' => 'phalapi/phalapi',
        'dev' => true,
    ),
    'versions' => array(
        'graham-campbell/result-type' => array(
            'pretty_version' => '1.0.x-dev',
            'version' => '1.0.9999999.9999999-dev',
            'type' => 'library',
            'install_path' => __DIR__ . '/../graham-campbell/result-type',
            'aliases' => array(),
            'reference' => 'ec4daa4b45faf6a683c9c0acf92fe1377d695a54',
            'dev_requirement' => false,
        ),
        'phalapi/cli' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phalapi/cli',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => 'c5df41e5fa483ad5990296421cde808a0d29f36f',
            'dev_requirement' => false,
        ),
        'phalapi/kernal' => array(
            'pretty_version' => '2.18.1',
            'version' => '2.18.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phalapi/kernal',
            'aliases' => array(),
            'reference' => '856f05ea852ee3619e298ffad7a42a89f74799e6',
            'dev_requirement' => false,
        ),
        'phalapi/notorm' => array(
            'pretty_version' => '2.10.0',
            'version' => '2.10.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phalapi/notorm',
            'aliases' => array(),
            'reference' => '337cac09d3563a325d574298b87c6a7a5f8f759f',
            'dev_requirement' => false,
        ),
        'phalapi/phalapi' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'fac1c4c77bf1c2c7b9849e19246b4976b9a44b8d',
            'dev_requirement' => false,
        ),
        'phalapi/qrcode' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phalapi/qrcode',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => 'cbb3c5f3f95a7902b0a72786a77495ada0c006b7',
            'dev_requirement' => false,
        ),
        'phalapi/task' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phalapi/task',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => '552421219a3097a81c576fc2607d96894d489def',
            'dev_requirement' => false,
        ),
        'phpoption/phpoption' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoption/phpoption',
            'aliases' => array(
                0 => '1.8.x-dev',
            ),
            'reference' => '5f59b1b59025b3c66cf1ca0898247d9de39c6200',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(
                0 => '1.26.x-dev',
            ),
            'reference' => '6fd1b9a79f6e3cf65f9e679b23af304cd9e010d4',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(
                0 => '1.26.x-dev',
            ),
            'reference' => '9344f9cb97f3b19424af1a21a3b0e75b0a7d8d7e',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(
                0 => '1.26.x-dev',
            ),
            'reference' => 'cfa0ae98841b9e461207c13ab093d76b0fa7bace',
            'dev_requirement' => false,
        ),
        'vlucas/phpdotenv' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../vlucas/phpdotenv',
            'aliases' => array(
                0 => '5.4.x-dev',
            ),
            'reference' => 'dd46c263f277573244c517bac125a78f67b83a98',
            'dev_requirement' => false,
        ),
    ),
);
